import { Component } from '@angular/core';
import { ProductDetail } from './interfaces/ProductoDetail.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'personal_learning';
  toBillProducts: ProductDetail[] = []

  addProduct(product: ProductDetail) {
    const index = this.checkIfProductIsInList(product.productoid);
    if (product.cantidad + 1 > product.stock){
      alert('Accion no valida, sobrepasa el stock');
    } else if (index >= 0){
      this.toBillProducts[index].cantidad += 1;
    } else {
      product.cantidad = 1;
      this.toBillProducts.push(product);
    }
  }

  checkIfProductIsInList(productId: number): number {
    const existingProduct = this.toBillProducts.find(p => p.productoid === productId);
    if (existingProduct) {
      return this.toBillProducts.findIndex(item => item.productoid === productId);
    } else {
      return -1; //-1 means the product is not in the list
    }
  }
}
