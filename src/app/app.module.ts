import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './lib/shared/shared.module';
import { ItemListComponent } from './lib/item-list/item-list.component';
import { NgIconsModule } from '@ng-icons/core';
import { heroTrash, heroPlus, heroEye } from '@ng-icons/heroicons/outline';
import { BillingComponent } from './lib/billing/billing.component';
import { BillDetailComponent } from './lib/bill-detail/bill-detail.component'
import { taxesPipe } from './pipes/TaxesPipe';
import { discountPipe } from './pipes/DiscountPipe';
import { ProductModalComponent } from './lib/product-modal/product-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    ItemListComponent,
    BillingComponent,
    BillDetailComponent,
    ProductModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    taxesPipe,
    discountPipe,
    NgIconsModule.withIcons({
      heroEye,
      heroTrash,
      heroPlus
    })
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
