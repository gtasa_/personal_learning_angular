import { Component, Input } from '@angular/core';
import { ProductDetail } from '../../interfaces/ProductoDetail.interface';

@Component({
  selector: 'app-bill-detail',
  templateUrl: './bill-detail.component.html',
})
export class BillDetailComponent {
  @Input() productList: ProductDetail[] = [];
}
