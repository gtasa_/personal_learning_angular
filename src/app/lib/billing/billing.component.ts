import { Component, Input } from '@angular/core';
import { ProductDetail } from '../../interfaces/ProductoDetail.interface';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
})
export class BillingComponent {
  @Input() productList: ProductDetail[] = [];

  discountValue: number = 2;

  getProductCount() {
    let count: number = 0
    this.productList.forEach(item => {
      count += item.cantidad
    })
    return count
  }

  getSubtotal() {
    let count: number = 0;
    this.productList.forEach(item => {
      count += item.precio * item.cantidad;
    })
    return count
  }

  getTax(subt: number) {
    return subt * 0.12
  }

  getTotal() {
    let subtwithdiscount = this.getSubtotal() - this.discount()
    let taxes = this.getTax(subtwithdiscount)
    return subtwithdiscount + taxes
  }

  discount(){
    let discountv = ((this.getSubtotal() * this.discountValue) / 100)
    return discountv
  }
}