import { Component, Output, EventEmitter, Input } from '@angular/core';
import { ProductDetail } from '../../interfaces/ProductoDetail.interface';
import { ProductModalService } from '../../services/product-modal-service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html'
})
export class ItemListComponent {
  @Input() toBillProducts: ProductDetail[] = [];

  @Output() 
  EmitProductEvent = new EventEmitter<ProductDetail>();

  constructor(private modalService: ProductModalService){ }

  addProductEnabled: boolean = true;

  productList: ProductDetail[] = [
    {
      productoid: 1,
      producto: 'Play Station 5',
      modelo: 'Ultra Slim',
      proveedor: 'H&B sa',
      precio: 600.00,
      stock: 40,
      cantidad: 0
    },
    {
      productoid: 2,
      producto: 'PC Gammer',
      modelo: 'AS-001-wm',
      proveedor: 'Asus',
      precio: 850.00,
      stock: 21,
      cantidad: 0
    },
    {
      productoid: 3,
      producto: 'MousePad',
      modelo: 'MP-2001-A',
      proveedor: 'Juan Marcet',
      precio: 10.00,
      stock: 3,
      cantidad: 0
    }
  ];

  eraseProductById(id: number) {
    let indexOfProduct = this.productList.findIndex(item => item.productoid === id);
    this.productList.splice(indexOfProduct, 1);
  }

  emitProduct(product: ProductDetail){
    this.EmitProductEvent.emit(product);
  }

  setThisProductToModal(product: ProductDetail){
    this.modalService.productModalTrigger.emit(product)
  }
}
