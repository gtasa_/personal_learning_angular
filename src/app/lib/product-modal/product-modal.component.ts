import { Component, Input } from '@angular/core';
import { ProductDetail } from '../../interfaces/ProductoDetail.interface';
import { ProductModalService } from '../../services/product-modal-service';

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
})
export class ProductModalComponent {
  constructor(private PModalService: ProductModalService){ }

  title: string = 'Detalle del producto'

  ngOnInit(){
    this.PModalService.productModalTrigger.subscribe(data => {
      console.log('Recibiendo producto', data)
    })
  }
}
