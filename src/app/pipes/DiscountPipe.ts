import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    standalone: true,
    name: 'discountPipe'
})
export class discountPipe implements PipeTransform {
    transform(value: number, discountValue: number) {
        return `$${Number(((value * discountValue) / 100 )).toFixed(2)}`
    }
}