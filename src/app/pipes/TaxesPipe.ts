import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    standalone: true,
    name: 'taxesPipe'
})
export class taxesPipe implements PipeTransform {
    transform(value: any) {
        return `$${Number(value * 0.12).toFixed(2)}`
    }
}