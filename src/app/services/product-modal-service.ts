import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductModalService {
  @Output() productModalTrigger: EventEmitter<any> = new EventEmitter()

  constructor() { }
}
